all:
	@find . -name '*.cpp' -execdir \
	  mex -R2018a CFLAGS='-W -Wall -O3' {} \;

clean:
	@find . -name '*~' -exec rm -f {} \;
