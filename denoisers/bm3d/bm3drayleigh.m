function img_fil = bm3drayleigh(img_nse, L)

    img_fil = exp(bm3dsc(log(img_nse), sqrt(psi(1, L))/2));
    img_fil = img_fil * sqrt(L / exp(psi(L)));
