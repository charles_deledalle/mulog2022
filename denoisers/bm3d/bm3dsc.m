function img_fil = bm3dsc(img_nse, sig)

    img_nse = double(img_nse);
    beta  = min(img_nse(:));
    alpha = (max(img_nse(:)) - min(img_nse(:))) / 255;
    img_nse = double((img_nse-beta)/alpha);
    img_fil = bm3d(img_nse, sig/alpha)*alpha+beta;
