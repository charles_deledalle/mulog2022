function lambda = dncnn_lambda_function()

folderModel = 'model';
if ~exist('vl_setupnn', 'file') || ...
        ~exist(fullfile(folderModel,'specifics',['sigma=10.mat']), 'file')
    disp('1. Please install DnCNN');
    fprintf('\n');
    disp('  git clone https://github.com/cszn/DnCNN.git');
    fprintf('\n');
    disp('2. Add DnCNN to your matlab path');
    fprintf('\n');
    disp('3. Next install Matconvnet and run');
    fprintf('\n');
    disp('  run (MATCONVNET)/matlab/vl_setupnn');
    fprintf('\n');
    error('Missing dependencies');
end

modelSigma_list  = 10:5:75;
for k = modelSigma_list
    modelSigma = k;
    data = load(fullfile(folderModel,'specifics',['sigma=',num2str(modelSigma,'%02d'),'.mat']));
    model.net{k} = vl_simplenn_tidy(data.net);
end
clear data

function x = dncnn_sub(y, sigma, varargin)

% Important to take quantiles vs min/max otherwise salt&pepper kill the dynamic
alpha = 0.001;
ymin = quantile(y(:), alpha);
ymax = quantile(y(:), 1-alpha);
Q = 1 / (ymax - ymin);
sig255 = sigma * Q * 255;

% Select best fitting model (important to take ceil instead of round)
modelSigma  = min(75, max(10, ceil(sig255/5)*5));
net = model.net{modelSigma};

% Rescale y to match model (important too!)
ratio = modelSigma / sig255;
ymin = ymin + (1 - 1/ratio) / Q;
ymax = ymin + 1/ratio / Q;
Q = Q * ratio;
y = (y - ymin) * Q;

% Run DnCNN on each channel
x = zeros(size(y));
options   = makeoptions(varargin{:});
cbwaitbar = getoptions(options, 'waitbar', @(dummy) []);
[m, n, K] = size(y);
parfor k = 1:K
    input = single(y(:,:,k));
    res = vl_simplenn(net,input,[],[],'conserveMemory',true,'mode','test');
    x(:,:,k) = input - res(end).x;
    x(:,:,k) = x(:,:,k) / Q + ymin;
    cbwaitbar((K-k+1) / K);
end
x(isnan(x)) = 0;
cbwaitbar(1);

end

lambda = @dncnn_sub;

end