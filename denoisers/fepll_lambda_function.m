function lambda = fepll_lambda_function()

folderModel = 'model';
if ~exist('fepll', 'file')
    disp('1. Please install FEPLL');
    fprintf('\n');
    disp('  git clone https://github.com/pshibby/fepll_public.git');
    fprintf('\n');
    disp('2. Add FEPLL to your matlab path');
    fprintf('\n');
    error('Missing dependencies');
end

model = get_prior_model();

function x = fepll_sub(y, sigma, varargin)

% Important to take quantiles vs min/max otherwise salt&pepper kill the dynamic
alpha = 0.001;
ymin = quantile(y(:), alpha);
ymax = quantile(y(:), 1-alpha);
Q = 1 / (ymax - ymin);

% Rescale y to match model (important too!)
sigma = sigma * Q;
y = (y - ymin) * Q;

% Run FEPLL on each channel
x = zeros(size(y));
options   = makeoptions(varargin{:});
cbwaitbar = getoptions(options, 'waitbar', @(dummy) []);
[m, n, K] = size(y);
parfor k = 1:K
    input = double(y(:,:,k));
    x(:,:,k) = fepll(input, sigma, model, ...
                     'clip', false, 'verbose', false, ...
                     'warning', 'off', varargin{:});
    x(:,:,k) = x(:,:,k) / Q + ymin;
    cbwaitbar((K-k+1) / K);
end
x(isnan(x)) = 0;
cbwaitbar(1);

end

lambda = @fepll_sub;

end