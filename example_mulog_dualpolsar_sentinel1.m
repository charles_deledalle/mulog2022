clear all
close all

addpathrec('.');

%% Load polsar image
C = loadsarimg('data/shanghai_sentinel1.dualpol.mat');

%% Setting
L            = 1;
[~, ~, D, ~] = size(C);
denoiser     = @bm3d;

%% Run MuLoG with embedded Gaussian denoiser
disp('Run MuLoG with embedded Gaussian denoiser');
tic;
h = robustwaitbar(0);
[ Shat xhat y ] = ...
    mulog(C, L, denoiser, ...
          'waitbar', @(p) robustwaitbar(p, h));
close(h);
disp(sprintf('  Elapsed time %.2f s', toc));

%% Display results
close all
f = fancyfigure;
subplot(4, 4, [1:2 5:6]);
h   = plotimagesar(C, 'beta', 3, 'alpha', 0.7, 'adjust', 'm3s');
title('noisy image');
subplot(4, 4, D + [1:2 5:6]);
plotimagesar(Shat, 'rangeof', h);
title('filtered image');
for k = 1:D
    for l = 1:D
        subplot(4, 4, 4*D + (k - 1) * 4 + l);
        plotimage(y(:, :, (k - 1) * D + l), 'range', [-6 6]);
        title(sprintf('noisy log channel %d', (k - 1) * D + l));
    end
end
for k = 1:D
    for l = 1:D
        subplot(4, 4, 4*D + D + (k - 1) * 4 + l);
        plotimage(xhat(:, :, (k - 1) * D + l), 'range', [-6 6]);
        title(sprintf('filtered log channel %d', (k - 1) * D + l));
    end
end
linkaxes;

%% Save figure
%savesubfig(f, '/tmp/result');
