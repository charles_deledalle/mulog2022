clear all
close all

addpathrec('.');

%% Load a polsar image
C = loadsarimg('data/tsukaba_pisar.mat');

%% Setting
L            = 2;
R            = 1;
[~, ~, D, ~] = size(C);
denoiser     = @bm3d;

%% Run MuLoG with embedded Gaussian denoiser
disp('Run MuLoG with embedded Gaussian denoiser');
tic;
h = robustwaitbar(0);
[ Shat xhat y ] = ...
    mulog(C, L, denoiser, ...
          'waitbar', @(p) robustwaitbar(p, h), ...
          'R', R);
close(h);
disp(sprintf('  Elapsed time %.2f s', toc));

%% Display results
close all
f = fancyfigure;
subplot(6, 6, [1:3 7:9 13:15]);
h   = plotimagesar(C, 'beta', 3, 'alpha', 0.7, 'adjust', 'm2s');
title('noisy image');
subplot(6, 6, D + [1:3 7:9 13:15]);
plotimagesar(Shat, 'rangeof', h);
title('filtered image');
for k = 1:D
    for l = 1:D
        subplot(6, 6, 6*D + (k - 1) * 6 + l);
        plotimage(y(:, :, (k - 1) * D + l), 'range', [-6 6]);
        title(sprintf('noisy log channel %d', (k - 1) * D + l));
    end
end
for k = 1:D
    for l = 1:D
        subplot(6, 6, 6*D + D + (k - 1) * 6 + l);
        plotimage(xhat(:, :, (k - 1) * D + l), 'range', [-6 6]);
        title(sprintf('filtered log channel %d', (k - 1) * D + l));
    end
end
linkaxes;

%% Save figure
%savesubfig(f, '/tmp/result');
