function h = plotimage(img, varargin)
%% Display an image
%
% Input/Output
%
%    img        a M x N array
%
%    h          handle on the created axes
%
% Optional arguments
%
%    range      range on wich pixel values are mapped to grey or
%               RGB values (default [0 255])
%
%    alpha      for gamma correction: beta * img.^alpha (default 1)
%
%    beta       for gamma correction: beta * img.^alpha (default 1)
%
%    Q          number of auqntification levels (default 256).
%               incompatible with on Windows systems
%
%    adjust     redefine the range automatically:
%               'no':   keep the original range
%               'auto': range is determined form extreme values
%               'm1s':  range is [0 m+s] where m and s are the mean
%                       and standard deviation of img
%               'm2s':  range is [0 m+2*s]
%               'm3s':  range is [0 m+3*s]
%               'm4s':  range is [0 m+4*s]
%               'm5s':  range is [0 m+5*s]
%
%    rangeof    handle to an existing axes (created by plotimage*)
%               used to display img with the exact same color palette.
%               all other optional arguments are ignored.
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr



  if nargin < 2
    options = struct([]);
  end

  import tools.*

  options   = makeoptions(varargin{:});
  adjust    = 'no';
  if isfield(options, 'rangeof')
      h     = getoptions(options, 'rangeof', [], true);
      range = getfield(get(h, 'UserData'), 'range');
      alpha = getfield(get(h, 'UserData'), 'alpha');
      beta  = getfield(get(h, 'UserData'), 'beta');
      Q     = getfield(get(h, 'UserData'), 'Q');
  else
      range = getoptions(options, 'range', [0 255]);
      if ~isfield(options, 'range')
          adjust = getoptions(options, 'Adjust', 'no');
      end
      alpha = getoptions(options, 'alpha', 1);
      beta  = getoptions(options, 'beta', 1);
      Q     = getoptions(options, 'Q', 256);
  end
  m = range(1);
  M = range(2);

  img = beta * img.^alpha;
  switch adjust
      case 'no'
      case 'auto'
          m = min(img(:));
          M = max(img(:));
      case 'm5s'
          m = 0;
          M = mean(img(:)) + 5*std(img(:));
      case 'm4s'
          m = 0;
          M = mean(img(:)) + 4*std(img(:));
      case 'm3s'
          m = 0;
          M = mean(img(:)) + 3*std(img(:));
      case 'm2s'
          m = 0;
          M = mean(img(:)) + 2*std(img(:));
      case 'm1s'
          m = 0;
          M = mean(img(:)) + 1*std(img(:));
      case 'm0s'
          m = 0;
          M = mean(img(:)) + 0*std(img(:));
      otherwise
          error(sprintf('Display adjustment %s unknown', adjust));
  end
  if m >= M
      m = -1;
      M = 1;
  end
  img = real(img);
  if size(img, 3) == 1
      h = imagesc(img);
      caxis([m M]);
      colormap(gray(Q));
  else
      img(img > M) = M;
      img(img < m) = m;
      h = imshow((img - m) / (M - m), [0 1]);
  end
  set(h, 'UserData', struct('range', [m, M], ...
                            'alpha', alpha, ...
                            'beta', beta, ...
                            'Q', Q));
  axis image;
  axis off;

  if nargout == 0
      clear h;
  end
