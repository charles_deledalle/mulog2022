function h = plotimagesar(img, varargin)
%% Display a SAR/PolSAR image
%
% Input/Output
%
%    img        a M x N array, or D x D x M x N matrix field
%
%    h          handle on the created axes
%
% Optional arguments
%
%    see plotimage
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2017 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr



if ndims(img) == 2
    [n1, n2] = size(img);
    img = reshape(img, [n1 n2 1 1]);
end
[n1, n2, D, D] = size(img);

switch D
    case 1
        h = plotimage(squeeze(sqrt(real(img))), 'Adjust', 'm3s', varargin{:});
    case 2
        rgb = zeros(n1, n2, 3);
        rgb(:,:,3) = img(:, :, 1, 1) + img(:, :, 2, 2) + 2 * real(img(:, :, 1, 2));
        rgb(:,:,1) = img(:, :, 1, 1) + img(:, :, 2, 2) - 2 * real(img(:, :, 1, 2));
        rgb(:,:,2) = img(:, :, 1, 1) + img(:, :, 2, 2);

        options = makeoptions(varargin{:});
        if isfield(options, 'rangeof')
            h = getoptions(options, 'rangeof', [], true);
            trgb = getfield(get(h, 'UserData'), 'trgb');
        else
            trgb = real([mean(mean(rgb(:,:,1)));
                         mean(mean(rgb(:,:,2)));
                         mean(mean(rgb(:,:,3)))]);
        end
        rgb(:,:,1) = rgb(:,:,1) / trgb(1);
        rgb(:,:,2) = rgb(:,:,2) / trgb(2);
        rgb(:,:,3) = rgb(:,:,3) / trgb(3);
        h = plotimage(sqrt(real(rgb)), varargin{:}, 'Adjust', 'm2s');
    case 3
        rgb = zeros(n1, n2, 3);
        rgb(:,:,1) = img(:, :, 1, 1) + img(:, :, 3, 3) - 2 * real(img(:, :, 1, 3));
        rgb(:,:,2) = sqrt(2) * img(:, :, 2, 2);
        rgb(:,:,3) = img(:, :, 1, 1) + img(:, :, 3, 3) + 2 * real(img(:, :, 1, 3));

        options = makeoptions(varargin{:});
        if isfield(options, 'rangeof')
            h = getoptions(options, 'rangeof', [], true);
            trgb = getfield(get(h, 'UserData'), 'trgb');
        else
            vect = @(x) x(:);
            trgb = real([quantile(vect(abs(rgb(:,:,1))), .95);
                         quantile(vect(abs(rgb(:,:,2))), .95);
                         quantile(vect(abs(rgb(:,:,3))), .95)]);
            trgb([1, 3]) = max(trgb([1, 3]));
            normtype = getoptions(options, 'normtype', 'classic');
            switch normtype
                case 'classic'
                    trgb([1, 3]) = max(trgb([1, 3]));
                case 'sync'
                    trgb(:) = max(trgb([1, 2, 3]));
                case 'indep'
                otherwise
                    error(sprintf('nomtype "%s" unknown', normtype));
            end
        end
        rgb(:,:,1) = rgb(:,:,1) / trgb(1);
        rgb(:,:,2) = rgb(:,:,2) / trgb(2);
        rgb(:,:,3) = rgb(:,:,3) / trgb(3);
        h = plotimage(sqrt(real(rgb)), varargin{:}, 'Adjust', 'm2s');
end

if D > 1
    set(h, 'UserData', setfield(get(h, 'UserData'), 'trgb', trgb));
end

if nargout == 0
    clear h;
end
