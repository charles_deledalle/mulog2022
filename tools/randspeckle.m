function [C, R] = randspeckle(varargin)
%% Generate a polsar image with speckle
%
% Examples:
%  C = randspeckle(S, L, [rho])
%  C = randspeckle(M, N, D, L)
%  C = randspeckle(M, N, D, T, L, [rho])
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2021 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr


switch nargin
    case 2
        S = varargin{1};
        L = varargin{2};
        [M, N, D, D, T] = size(S);
        if T == 1
            C = randspeckle(M, N, D, L);
            Sh = spfunmatrices(S, @(x) sqrt(x));
            C  = mulmatrices(Sh, C, Sh, 'HNhhh');
        else
            C = zeros(M, N, D, D, T);
            for t = 1:T
                C(:, :, :, :, t) = randspeckle(S(:, :, :, :, t), L);
            end
        end
    case 3
        S = varargin{1};
        L = varargin{2};
        rho = varargin{3};
        [M, N, D, D, T] = size(S);
        [C, R] = randspeckle(M, N, D, T, L, rho);
        for t = 1:T
            Sh = spfunmatrices(S(:, :, :, :, t), @(x) sqrt(x));
            C(:, :, :, :, t)  = mulmatrices(Sh, C(:, :, :, :, t), Sh, 'HNhhh');
        end
    case 4
        M = varargin{1};
        N = varargin{2};
        D = varargin{3};
        L = varargin{4};
        T = 1;
        C = zeros(M, N, D, D);
        K = (randn(M, N, D, L) + sqrt(-1) * randn(M, N, D, L)) / sqrt(2);
        for k = 1:D
            for l = 1:D
                C(:, :, k, l) = mean(K(:, :, k, :) .* conj(K(:, :, l, :)), 4);
            end
        end
    case 5
        M = varargin{1};
        N = varargin{2};
        D = varargin{3};
        T = varargin{4};
        L = varargin{5};
        C = zeros(M, N, D, D, T);
        for t = 1:T
            C(:, :, :, :, t) = randspeckle(M, N, D, L);
        end
    case 6
        M = varargin{1};
        N = varargin{2};
        D = varargin{3};
        T = varargin{4};
        L = varargin{5};
        rho = varargin{6};

        if ischar(rho)
            switch rho
                case 'random'
                    % Generate random correlation profile
                    R = zeros(1, 1, T, T);
                    for i = 1:T
                        for j = 1:T
                            R(1, 1, i, j) = exp(-abs(i - j) / 4); % Laplacian decay
                        end
                    end
                    R = betarnd(R, 1 - R);
                case 'periodical'
                    R = zeros(1, 1, T, T);
                    for i = 1:T
                        for j = 1:T
                            if mod(i - j, 4) == 0
                                R(1, 1, i, j) = .5;
                            end
                        end
                    end
            end
            for i = 1:T
                R(1, 1, i, (i+1):end) = R(1, 1, (i+1):end, i);
                R(1, 1, i, i) = 1;
            end
            [E, La] = eigmatrices(R);
            R = mulmatrices(E, max(La, 0), adjmatrices(E), 'HNndn');
            Nsi = diagmatrices(1 ./ sqrt(diagmatrices(R)));
            R = real(mulmatrices(Nsi, R, Nsi, 'HNhhh'));
            R = max(R, 0);
        elseif isscalar(rho)
            % Constant correlation
            rho = sqrt(rho);
            R = rho + (1-rho) * reshape(eye(T, T), [1, 1, T, T]);
        else
            % Prescribed correlation profile
            R = reshape(rho, [1, 1, T, T]);
        end
        R_sqrt = spfunmatrices(R, @(x) sqrt(x));
        R_sqrt = repmat(R_sqrt, [M, N, 1, 1]);
        C = zeros(M, N, D, D, T);
        for i = 1:L
            K = (randn(M, N, T, D) + sqrt(-1) * randn(M, N, T, D)) / sqrt(2);
            for k = 1:D
                K(:, :, :, k) = mulmatrices(R_sqrt, K(:, :, :, k), 'Nhn');
            end
            K1 = permute(K, [1, 2, 4, 5, 3]);
            K2 = permute(K, [1, 2, 5, 4, 3]);
            C = C + K1 .* conj(K2);
        end
        C = C / L;
end

if nargout == 2
    if ~exist('R', 'var');
        R = eye(T);
    else
        R = squeeze(R);
    end
end