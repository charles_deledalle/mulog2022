function S = rgb2polsar(rgb)
%% Generate a polsar image from an RGB image
%
%
% License
%
% This software is governed by the CeCILL license under French law and
% abiding by the rules of distribution of free software. You can use,
% modify and/ or redistribute the software under the terms of the CeCILL
% license as circulated by CEA, CNRS and INRIA at the following URL
% "http://www.cecill.info".
%
% As a counterpart to the access to the source code and rights to copy,
% modify and redistribute granted by the license, users are provided only
% with a limited warranty and the software's author, the holder of the
% economic rights, and the successive licensors have only limited
% liability.
%
% In this respect, the user's attention is drawn to the risks associated
% with loading, using, modifying and/or developing or reproducing the
% software by the user in light of its specific status of free software,
% that may mean that it is complicated to manipulate, and that also
% therefore means that it is reserved for developers and experienced
% professionals having in-depth computer knowledge. Users are therefore
% encouraged to load and test the software's suitability as regards their
% requirements in conditions enabling the security of their systems and/or
% data to be ensured and, more generally, to use and operate it in the
% same conditions as regards security.
%
% The fact that you are presently reading this means that you have had
% knowledge of the CeCILL license and that you accept its terms.
%
% Copyright 2021 Charles Deledalle
% Email charles-alban.deledalle@math.u-bordeaux.fr


if ndims(rgb) == 4
    [M, N, D, T] = size(rgb);
    S = zeros(M, N, D, D, T);
    for t = 1:T
        S(:, :, :, :, t) = rgb2polsar(rgb(:, :, :, t));
    end
    return
end
[M, N, D] = size(rgb);
rgb = imresize(rgb, .99);
rgb = imresize(rgb, [M, N]);
rgb = (rgb - min(rgb(:))) / (max(rgb(:)) - min(rgb(:))) + 1 / 256;
rgb = rgb.^2;
[M, N, D] = size(rgb);
S = zeros(M, N, D, D);
S(:, :, 1, 1) = (sqrt(rgb(:, :, 3)) + sqrt(rgb(:, :, 1))).^2 / 4;
S(:, :, 2, 2) = rgb(:, :, 2) / sqrt(2);
S(:, :, 3, 3) = (sqrt(rgb(:, :, 3)) - sqrt(rgb(:, :, 1))).^2 / 4;
S(:, :, 1, 3) = (rgb(:, :, 3) - rgb(:, :, 1)) / 4 * (1 + i);
S(:, :, 1, 2) = 0;
S(:, :, 2, 3) = 0;
S(:, :, 2, 1) = conj(S(:, :, 1,2));
S(:, :, 3, 1) = conj(S(:, :, 1,3));
S(:, :, 3, 2) = conj(S(:, :, 2,3));
S = stabmatrices(S);
[U, La] = eigmatrices(S);
La = abs(La);
UT = adjmatrices(U);
S = mulmatrices(U, La, UT, 'HNndn');
S = condloading(S, 1e-3);
